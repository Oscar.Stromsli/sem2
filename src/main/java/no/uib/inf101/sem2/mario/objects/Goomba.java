package no.uib.inf101.sem2.mario.objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class Goomba extends GameObject {

    public static final String GOOMBA_FILE = "Goomba.png";
    

    public static int GOOMBA_WIDTH = 32;
    public static int GOOMBA_HEIGHT = 32;

    public static int VEL_X = 2;
    public static int VEL_Y = 0;



    public boolean dead = false;

    public BufferedImage goombaImage;

    public Goomba(int levelWidth, int levelHeight, int startPosition) {
        super(VEL_X, VEL_Y, startPosition + (Ground.BLOCK_SIZE * 8) + (int)(Math.random() * 500) , levelHeight - GOOMBA_HEIGHT - Ground.BLOCK_SIZE,
                levelWidth - GOOMBA_WIDTH, levelHeight - GOOMBA_HEIGHT - Ground.BLOCK_SIZE,
                     GOOMBA_WIDTH, GOOMBA_HEIGHT, levelWidth, levelHeight, Direction.RIGHT);

        try {
            if (goombaImage == null) {

                InputStream goomba = getClass().getClassLoader().getResourceAsStream(GOOMBA_FILE);
                if (goomba != null) {
                    goombaImage = ImageIO.read(goomba);
                    goomba.close();
                } else {
                    throw new IOException("Error: " + GOOMBA_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }


    @Override
    public void move() {
        x += VEL_X; // Update Goomba's x position with the ENEMY_SPEED_X value
    
        // Check if Goomba collides with the left or right boundary of the level
        if (x <= 0 || x >= levelWidth - width) {
            // Reverse Goomba's direction
            VEL_X = -VEL_X; // Update ENEMY_SPEED_X to reverse direction
            // Update Goomba's direction to left or right based on velocity
            if (VEL_X > 0) {
                direction = Direction.RIGHT;
            } else {
                direction = Direction.LEFT;
            }
        }
        handleOffScreen();
    }
    

    @Override
    public void handleOffScreen() {
        if (x < 0) {
            x = 0;
        } else if (x > maxX) {
            x = maxX;
        }

        if (y < 0) {
            y = 0;
        } else if (y > maxY) {
            y = maxY;
        }
    }

    public void changeDirection() {
        VEL_X -= VEL_X;
    }

    public void draw(Graphics g) {
        if (!dead) {
            g.drawImage(goombaImage, x, y, width, height, null);
        }
    }  
}
