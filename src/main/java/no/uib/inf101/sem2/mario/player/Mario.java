package no.uib.inf101.sem2.mario.player;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.objects.GameObject;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class Mario extends GameObject {

    public static final String STANDING_MARIO_FILE = "MarioStanding.png";
    // public static final String JUMPING_MARIO_FILE = "MarioJumping.png";
    public static final String SUPER_MARIO_STANDING_FILE = "SuperMarioStanding.png";
    // public static final String SUPER_MARIO_JUMPING_FILE = "SuperMarioJumping.png";

    
    public static int MARIO_HEIGHT = 32;
    public static int MARIO_WIDTH = 26;
    // public static int SUPER_MARIO_HEIGHT = 64;
    // public static int SUPER_MARIO_WIDTH = 32;

    public static final int INIT_X = Ground.BLOCK_SIZE * 4;
    public static final int VEL_X = 0;
    public static final int VEL_Y = 0;
    public static final int MAX_SPEED = 3;
    public static final int GRAVITY = 1;

    private BufferedImage marioImage;

    public boolean jumping = false;
    public boolean maxHeight = false;
    public boolean dead = false;
    public boolean superMario = false;

    public Mario(int levelWidth, int levelHeight) {
        super(VEL_X, VEL_Y, INIT_X, levelHeight - MARIO_HEIGHT - Ground.BLOCK_SIZE,
                levelWidth - MARIO_WIDTH, levelHeight - MARIO_HEIGHT - Ground.BLOCK_SIZE,
                    MARIO_WIDTH, MARIO_HEIGHT, levelWidth, levelHeight, Direction.RIGHT);

        try {
            
            if (marioImage == null) {

                //Load files from resource
                InputStream standingMario = getClass().getClassLoader().getResourceAsStream(STANDING_MARIO_FILE);
                if (standingMario != null) {
                    marioImage = ImageIO.read(standingMario);
                    standingMario.close();
                } else {
                    throw new IOException("Error: " + STANDING_MARIO_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    @Override
    public void move() {

        if (jumping) {
            y += velY;
            velY += GRAVITY;

            if (y > maxY) {
                y = maxY;
                velY = 0;
                jumping = false;
            }
        } 

        // System.out.println("Mario's X position: " + x + ", Mario's Y position: " + y);
        determineMarioSprite();

        // Update x position
        handleOffScreen();

        if (Math.abs(velX) <= MAX_SPEED) {
            x += velX;
        } else {
            velX = velX > 0 ? MAX_SPEED : -MAX_SPEED;
            x += velX;
        }
    }

    @Override
    public void handleOffScreen() {
        
        // Stop Mario from escaping
        if (x < 0) {
            x = 0;
        } else if (x > maxX) {
            x = maxX;
        }

        if (y <= 0) {
            y = 0;
        } else if (y > maxY) {
            y = maxY;
        }       
    }

    public void determineMarioSprite() {
        if (superMario) {
            try {
                InputStream superMarioStanding = getClass().getClassLoader().getResourceAsStream(SUPER_MARIO_STANDING_FILE);
                if (superMarioStanding != null) {
                    marioImage = ImageIO.read(superMarioStanding);
                    superMarioStanding.close();
                } else {
                    throw new IOException("Error: " + SUPER_MARIO_STANDING_FILE);
                }
                // Increase Mario's size
                MARIO_HEIGHT = 64;
                MARIO_WIDTH = 32;
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        } else {
            try {
                InputStream standingMario = getClass().getClassLoader().getResourceAsStream(STANDING_MARIO_FILE);
                if (standingMario != null) {
                    marioImage = ImageIO.read(standingMario);
                    standingMario.close();
                } else {
                    throw new IOException("Error: " + STANDING_MARIO_FILE);
                }
                // Reset Mario's size to default
                MARIO_HEIGHT = 32;
                MARIO_WIDTH = 26;
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
    
      
    
    public void draw(Graphics g) {
        if (!dead) {
            g.drawImage(marioImage, x, y, width, height, null);
        }
    }
}