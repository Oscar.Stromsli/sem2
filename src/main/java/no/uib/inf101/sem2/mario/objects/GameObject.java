package no.uib.inf101.sem2.mario.objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import no.uib.inf101.sem2.mario.utilities.Direction;

/**
 * Abstract class representing game objects.
 */
public abstract class GameObject {

    public int x; // X-coordinate of the object
    public int y; // Y-coordinate of the object
    public int width; // Width of the object
    public int height; // Height of the object
    public int velX; // Velocity of the object along the X-axis
    public int velY; // Velocity of the object along the Y-axis
    public int maxX; // Maximum allowed X-coordinate of the object
    public int maxY; // Maximum allowed Y-coordinate of the object
    public int levelWidth; // Width of the game level
    public int levelHeight; // Height of the game level
    public Direction direction; // Direction of the object
    protected BufferedImage image; // Image representing the object

    public GameObject(int velX, int velY, int x, int y, int maxX, int maxY, int width, int height, int levelWidth, int levelHeight, Direction direction) {
        this.velX = velX;
        this.velY = velY;
        this.x = x;
        this.y = y;
        this.maxX = maxX;
        this.maxY = maxY;
        this.width = width;
        this.height = height;
        this.levelWidth = levelWidth;
        this.levelHeight = levelHeight;
        this.direction = direction;
    }

    /**
     * Abstract method for moving the game object.
     */
    public abstract void move();

    /**
     * Abstract method for handling off-screen behavior of the game object.
     */
    public abstract void handleOffScreen();

    public boolean intersects(GameObject obj) {
        return (intersectsTop(obj) || intersectsBottom(obj) || intersectsRight(obj) || intersectsLeft(obj));
    }

    public boolean intersectsTop(GameObject obj) {
        return (obj.y + obj.height >= y
                && obj.y + obj.height <= y + ((double) height / 2)
                && obj.x + obj.width >= x
                && obj.x <= x + width
                );
    }

    public boolean intersectsBottom(GameObject obj) {
        return (obj.y <= y + height
                && obj.y > y + ((double) height / 2)
                && obj.x + obj.width >= x
                && obj.x <= x + width
                );
    }

    public boolean intersectsRight(GameObject obj) {
        return (obj.x <= x + width
                && obj.x >= x + ((double) width / 2)
                && obj.y + obj.height >= y
                && obj.y <= y + height
                );
    }

    public boolean intersectsLeft(GameObject obj) {
        return (obj.x + obj.width >= x
                && obj.x + obj.width <= x + ((double) width / 2)
                && obj.y + obj.height >= y
                && obj.y <= y + height
                );
    }  

    public boolean collides(GameObject obj) {
        return (collidesTop(obj) || collidesBottom(obj) || collidesLeft(obj) || collidesRight(obj));
    }

    public boolean collidesTop(GameObject obj) {
        return (obj.y + obj.height == y
                && obj.x + obj.width >= x
                && obj.x <= x + width
                );
    }

    public boolean collidesBottom(GameObject obj) {
        return (obj.y == y + height
                && obj.x + obj.width >= x
                && obj.x <= x + width
                );
    }

    public boolean collidesRight(GameObject obj) {
        return (obj.x == x + width
                && obj.y + obj.height >= y
                && obj.y <= y + height
                );
    }

    public boolean collidesLeft(GameObject obj) {
        return (obj.x + obj.width >= x
                && obj.y + obj.height >= y
                && obj.y <= y + height
                );
    }    

    /**
     * Draw the game object on the graphics object.
     *
     * @param g The graphics object to draw on
     */
    public void draw(Graphics g) {
        g.drawImage(image, x, y, width, height, null);
    }
}