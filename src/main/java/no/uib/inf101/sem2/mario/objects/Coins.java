package no.uib.inf101.sem2.mario.objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.utilities.Direction;

public class Coins extends GameObject {

    public static String COIN_FILE = "Coin.png";


    public int startPosition;
    public static int VEL_X = 0;
    public static int VEL_Y = 0;
    public static final int COIN_WIDTH = 16;
    public static final int COIN_HEIGHT = 24;
    public static final int INIT_X = (COIN_HEIGHT * 16);

    private BufferedImage coinImage;


    public boolean picked = false;

    public Coins(int levelWidth, int levelHeight, int startPosition) {
        super(VEL_X, VEL_Y, startPosition, INIT_X, levelWidth, levelHeight, COIN_WIDTH, COIN_HEIGHT, levelWidth, levelHeight, Direction.LEFT);
    
        try {
            if (coinImage == null) {
                InputStream coin = getClass().getClassLoader().getResourceAsStream(COIN_FILE);
                if (coin != null) {
                    coinImage = ImageIO.read(coin);
                    coin.close();
                } else {
                    throw new IOException("Error: " + COIN_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    @Override
    public void move() {
    }

    @Override
    public void handleOffScreen() {
    }

    public void draw(Graphics g) {
        if (picked == false) {
            g.drawImage(coinImage, x, y, width, height, null);
        }
    }
    
}
