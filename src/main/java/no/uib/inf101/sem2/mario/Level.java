package no.uib.inf101.sem2.mario;

import no.uib.inf101.sem2.mario.blocks.BrickBlock;
import no.uib.inf101.sem2.mario.blocks.QuestionBlock;
import no.uib.inf101.sem2.mario.objects.Goomba;
import no.uib.inf101.sem2.mario.objects.Mushroom;

/**
 * Represents a level in a game, containing multiple ground objects.
 */
public class Level {

    private Ground[] ground; // Array of Ground objects representing the ground in the level
    private QuestionBlock[] questionBlocks;
    private BrickBlock[] brickBlocks;
    private Goomba[] goomba;
    private Mushroom[] mushroom;

    public Level(int groundWidth, int levelWidth, int levelHeight) {

        // Initialize the Ground array with the desired groundWidth and groundHeight
        ground = new Ground[groundWidth];
        for (int i = 0; i < ground.length; i++) {
            ground[i] = new Ground(levelWidth, levelHeight, i * Ground.BLOCK_SIZE, levelHeight - Ground.BLOCK_SIZE);
        }
        
        // Initialize the QuestionBlock array with the desired number of question blocks
        questionBlocks = new QuestionBlock[3]; // Change to the desired number of question blocks
        for (int i = 0; i < questionBlocks.length; i++) {
            int startPosition = i * (QuestionBlock.BLOCK_SIZE * 16); // Change to the desired gap between question blocks
            questionBlocks[i] = new QuestionBlock(levelWidth, levelHeight, startPosition);
        }

        // Initialize the BrickBlock array with the designated number of brick blocks
        brickBlocks = new BrickBlock[5]; // Change to the desired number of question blocks
        for (int i = 0; i < brickBlocks.length; i++) {
            int startPosition = i * (BrickBlock.BLOCK_SIZE); // Change to the desired gap between question blocks
            brickBlocks[i] = new BrickBlock(levelWidth, levelHeight, startPosition);
        }

        // Initialize the Goomba array with the desired number of goombas
        goomba = new Goomba[3]; // Change to the desired number of goombas
        for (int i = 0; i < goomba.length; i++) {
            int startPosition = i * (Ground.BLOCK_SIZE + 4); // Change to the desired gap between goombas
            goomba[i] = new Goomba(levelWidth, levelHeight, startPosition);
        }

        mushroom = new Mushroom[3];
        for (int i = 0; i < mushroom.length; i++) {
            int startPosition = i * (QuestionBlock.BLOCK_SIZE * 16);
            mushroom[i] = new Mushroom(levelWidth, levelHeight, startPosition);
        }
        
    }

    public QuestionBlock[] getQuestionBlocks() {
        return questionBlocks.clone();
    }

    public Ground[] getGround() {
        return ground.clone();
    }

    public BrickBlock[] getBrickBlocks() {
        return brickBlocks.clone();
    }

    public Goomba[] getGoombas() {
        return goomba.clone();
    }

    public Mushroom[] getMushrooms() {
        return mushroom.clone();
    }
}
