package no.uib.inf101.sem2.mario;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.Timer;
import javax.swing.JPanel;

import no.uib.inf101.sem2.mario.blocks.BrickBlock;
import no.uib.inf101.sem2.mario.blocks.QuestionBlock;
import no.uib.inf101.sem2.mario.objects.Castle;
import no.uib.inf101.sem2.mario.objects.Coins;
import no.uib.inf101.sem2.mario.objects.Goomba;
import no.uib.inf101.sem2.mario.objects.Mushroom;
import no.uib.inf101.sem2.mario.player.Mario;
import no.uib.inf101.sem2.mario.player.MarioController;
import no.uib.inf101.sem2.mario.utilities.HighScore;

/**
 * Represents the view for the Mario game, extending JPanel.
 */
public class MarioView extends JPanel {

    private Mario mario; // The Mario Character representing Player
    private Goomba[] goombas; // Array of Goomba characters
    private Level level; // Level object representing the game level
    private Ground[] grounds; // Array of Ground objects representing the grounds in the level
    private ArrayList<QuestionBlock> qBlocks;
    private ArrayList<BrickBlock> bricks;
    private ArrayList<Mushroom> mushroom;
    private Castle castle;
    private Coins coins;
    private HighScore highScore;

    // CONSTANTS
    public static final int LEVEL_WIDTH = 1408; // Width of the level in pixels
    public static final int LEVEL_HEIGHT = 600; // Height of the level in pixels
    public static final int GROUND_X = 0; // X-coordinate of the grounds tiles
    public static final int GROUND_Y = 600; // Y-coordinate of the grounds tiles

    public static final int MARIO_SPEED_X = 2; // Horizontal speed
    public static int MARIO_SPEED_Y = 16; // Vertical speed
    public static int ENEMY_SPEED_X = 2;

    // Frames per interval (But i will call it Frame per second)
    private static final int FPS = 60;

    public int score;

    public boolean gameWon;
    public boolean gameOver;
    public boolean gameIsRunning;

    /**
     * Constructor for MarioView class.
     */
    public MarioView() {

        highScore = new HighScore();
        
        // Initialize the Level object
        level = new Level(LEVEL_WIDTH / Ground.BLOCK_SIZE, LEVEL_WIDTH, LEVEL_HEIGHT);

        // Initialize the Ground array
        grounds = level.getGround();

        // Initialize the Castle
        castle = new Castle(LEVEL_WIDTH, LEVEL_HEIGHT, (grounds.length - 1) * Ground.BLOCK_SIZE - Castle.CASTLE_WIDTH);

        bricks = new ArrayList<BrickBlock>(Arrays.asList(level.getBrickBlocks()));

        // Initialize the ArrayList for storing QuestionBlock objects
        qBlocks = new ArrayList<QuestionBlock>(Arrays.asList(level.getQuestionBlocks()));
        
        mushroom = new ArrayList<Mushroom>(Arrays.asList(level.getMushrooms()));

        // Set the preferred size of MarioView panel
        setPreferredSize(new Dimension(1408, 600));
    
        // Create a new Mario Object
        mario = new Mario(LEVEL_WIDTH, GROUND_Y);

        coins = new Coins(LEVEL_WIDTH, LEVEL_HEIGHT, ABORT);

        // Create an array of new Goombas Objects
        goombas = new Goomba[3];
        for (int i = 0; i < 3; i++) {
            goombas[i] = new Goomba(LEVEL_WIDTH, LEVEL_HEIGHT, 0);
        }

        // Create a new MarioController and add it as a keyListener
        MarioController controller = new MarioController(mario);
        addKeyListener(controller);
        setFocusable(true);

        ActionListener movement = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {

                for (QuestionBlock qBlock : qBlocks) {
                    qBlock.move();
                }
                for (BrickBlock brickBlock : bricks) {
                    brickBlock.move();
                }

                mario.move();
                castle.move();
                coins.move();

                for (Goomba goomba : goombas) {
                    goomba.move();
                }

                highScore.updateScore(score);

                checkCollision();
                repaint();
            }
        };

        Timer timer = new Timer(1000 / FPS, movement);
        timer.start();

    }
    

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Draw the background
        g.setColor(Color.CYAN); // Example color for background
        g.fillRect(0, 0, getWidth(), getHeight()); // Example fill rectangle for background

        // Display one row of grounds tiles
        for (int i = 0; i < grounds.length; i++) {
            int tileLeft = i * Ground.BLOCK_SIZE;
            int tileTop = MarioView.LEVEL_HEIGHT - (+ 1) * Ground.BLOCK_SIZE;
            if (tileLeft < LEVEL_WIDTH && tileTop > 0) {
                grounds[i].draw(g);
            }
        }

        castle.draw(g);
        
        for (BrickBlock brickBlock : bricks) {
            brickBlock.draw(g);
        }  

        // Draw Question Block
        for (QuestionBlock qBlock : qBlocks) {
            qBlock.draw(g);
        }

        // Draw Mushroom object
        for (Mushroom sopp : mushroom) {
            sopp.draw(g);
        }

        // Draw Goombas
        for (Goomba goomba : goombas) {
            goomba.draw(g);
        }
        
        coins.draw(g);

        // Draw Mario
        mario.draw(g);
        repaint();
    }

    private void checkCollision() {

        for (Goomba goomba : goombas) {
            if (!mario.dead && !goomba.dead && goomba.intersectsTop(mario)) {
                goomba.dead = true;
                score += 100;
            } else if (!mario.dead && !goomba.dead && (goomba.intersectsLeft(mario) || goomba.intersectsRight(mario))) {
                if (mario.superMario == true) {
                    mario.superMario = false;
                } else {
                    mario.dead = true;
                }
            }
        }

        for (QuestionBlock questionBlock : qBlocks) {
            if (questionBlock.intersectsBottom(mario)) {
                questionBlock.hit = true;
                // Resolve collision - stop Mario's upward velocity and set his position just above the questionBlock
                mario.velY = 0;
                mario.y = questionBlock.y + mario.height + 1;
                mario.jumping = false; // Set jumping to false when Mario is on a surface

                if (!questionBlock.empty) {
                    questionBlock.createPowerUp();
                    questionBlock.empty = true;
                }

            } else if (questionBlock.intersectsLeft(mario) || questionBlock.intersectsRight(mario)) {
                // Resolve collision - stop Mario's horizontal velocity and set his position outside the questionBlock
                if (questionBlock.intersectsLeft(mario)) {
                    if (mario.velX < 0) {
                        mario.velX = 0;
                    }
                    mario.x = questionBlock.x - questionBlock.width; // Add a small offset to move Mario outside the questionBlock
                } else if (questionBlock.intersectsRight(mario)) {
                    if (mario.velX > 0) {
                        mario.velX = 0;
                    }
                    mario.x = questionBlock.x + mario.width; // Subtract a small offset to move Mario outside the questionBlock
                }
            } else if (questionBlock.intersectsTop(mario)) {
                mario.velY = 0;
                mario.y = questionBlock.y - mario.height;
                mario.jumping = false; // Set jumping to false when Mario is on a surface
            } else {
                // No collision with any surface, set jumping to true
                mario.jumping = true;
            }
            
        } 
        for (BrickBlock brickBlock : bricks) {
            if (brickBlock.hit == false) {
                if (brickBlock.intersectsBottom(mario)) {
                    if (mario.superMario == true) {
                        brickBlock.hit = true;
                    } else {
                        mario.velY = 0;
                        mario.y = brickBlock.y + mario.height + 1;
                        mario.jumping = false;
                    }
                } else if (brickBlock.intersectsTop(mario)) {
                    mario.velY = 0;
                    mario.y = brickBlock.y - mario.height;
                    mario.jumping = false;
                } else if (brickBlock.intersectsRight(mario)) {
                    if (mario.velX > 0) {
                        mario.velX = 0;
                    }
                }
            }
        }

        for (Mushroom sopp : mushroom) {
            if (sopp.intersects(mario)) {
                mario.superMario = true;
                sopp.eaten = true;
            }
        }

        if (castle.collides(mario)) {
            gameWon = true;
            System.out.println("You Won! Your score: " + score);
        } else if (mario.dead == true) {
            gameOver = true;
            System.out.println("You Lost! Your score: " + score);
        }

        if (coins.collides(mario) && coins.picked == false) {
            coins.picked = true;
            score += 100;
        }
    }


    public Component getHighScore() {
        return highScore;
    }
}

