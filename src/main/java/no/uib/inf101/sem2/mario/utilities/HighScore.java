package no.uib.inf101.sem2.mario.utilities;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class HighScore extends JPanel {
    private JLabel scoreLabel;

    public HighScore() {
        setOpaque(false);
        setLayout(new FlowLayout(FlowLayout.RIGHT));

        scoreLabel = new JLabel("Score: 0");
        add(scoreLabel);
    }

    public void updateScore(int score) {
        scoreLabel.setText("Score: " + score);
    }
}