package no.uib.inf101.sem2.mario;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.objects.GameObject;
import no.uib.inf101.sem2.mario.player.Mario;
import no.uib.inf101.sem2.mario.utilities.Direction;

/**
 * Ground class represents the ground objects in the game. 
 * It extends the GameObject class and inherits its properties and methods. 
 * Ground objects are used as platforms on which other game objects can stand.
 */
public class Ground extends GameObject {

    // Image file path for the ground
    public static final String filename = "Ground.png";

    // Size of each block of ground
    public static final int BLOCK_SIZE = 32;

    // Initial velocities
    public static int INIT_VEL_X = 0;
    public static final int INIT_VEL_Y = 0;

    // Current velocity in x-direction
    public static int vel_x;

    // Image buffer for the ground
    private static BufferedImage image;

    public Ground(int levelWidth, int levelHeight, int initX, int initY) {

        // Call superclass constructor with appropriate parameters
        super(INIT_VEL_X, INIT_VEL_Y, initX, initY, levelWidth - BLOCK_SIZE, levelHeight - BLOCK_SIZE, 
                BLOCK_SIZE, BLOCK_SIZE, levelWidth, levelHeight, Direction.LEFT);

        try {
            if (image == null) {

                // Load image file from resource
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
                if (inputStream != null) {
                    image = ImageIO.read(inputStream);
                    inputStream.close();
                } else {
                    throw new IOException("Failed to load image: " + filename);
                }
            }
        } catch (IOException e) {
            System.out.println("Internal Error:" + e.getMessage());
        }
    }

    @Override
    public void move() {
        // Ground does not move
    }

    public boolean collidesWithMario(Mario mario) {
        return collides(mario);
    }

    /**
     * HandleOffScreen method wraps the x-coordinate of the ground object around when it goes off the left or right edge
     * of the game court, creating an illusion of continuous scrolling.
     */
    @Override
    public void handleOffScreen() {

        // Wrap x-coordinate around when it goes off the left or right edge
        if (x < -BLOCK_SIZE) {
            x = levelWidth;
        } else if (x > levelWidth) {
            x = -BLOCK_SIZE;
        }
    }

    /**
     * Draw method draws the ground object using the Graphics object.
     * 
     * @param g Graphics object used for drawing
     */
    @Override
    public void draw(Graphics g) {
        
        // Draw the ground image using the Graphics object
        g.drawImage(image, x, y, width, height, null);
    }
}
