package no.uib.inf101.sem2.mario.objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class Mushroom extends GameObject {
    
    public static final String MUSHROOM_FILE = "Mushroom.png";

    public static final int MUSHROOM_SIZE = 32;

    public static int startPosition;
    public static int VEL_X = 2;
    public static int VEL_Y = 0;
    public static final int GRAVITY = 1;

    private BufferedImage mushroomImage;

    public boolean spawned = false;
    public boolean eaten = false;

    public Mushroom(int levelWidth, int levelHeight, int startPosition) {
        super(VEL_X, VEL_Y, startPosition, levelHeight - (MUSHROOM_SIZE * 4)- Ground.BLOCK_SIZE,
                levelWidth - MUSHROOM_SIZE, levelHeight - MUSHROOM_SIZE - Ground.BLOCK_SIZE,
                    MUSHROOM_SIZE, MUSHROOM_SIZE, levelWidth, levelHeight, Direction.RIGHT);

        try {
            if (mushroomImage == null) {
                InputStream mushroom = getClass().getClassLoader().getResourceAsStream(MUSHROOM_FILE);
                if (mushroom != null) {
                    mushroomImage = ImageIO.read(mushroom);
                    mushroom.close();
                } else {
                    throw new IOException("Error :" + MUSHROOM_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    @Override
    public void move() {
        
        System.out.println("Mushroom's X position : " + x + ", Mushroom's Y position: " + y);

        handleOffScreen();
    }


    @Override
    public void handleOffScreen() {
            
        // Stop Mario from escaping
        if (x < 0) {
            x = 0;
        } else if (x > maxX) {
            x = maxX;
        }

        if (y <= 0) {
            y = 0;
        } else if (y > maxY) {
            y = maxY;
        }       
    }

    public void draw(Graphics g) {
        if (eaten == false && spawned == false) {
            g.drawImage(mushroomImage, x, y, width, height, null); 
            System.out.println("Mushroom is visible");
        }
    }
}
