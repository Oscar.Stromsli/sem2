package no.uib.inf101.sem2.mario.blocks;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.objects.GameObject;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class BrickBlock extends GameObject {
    
    public static final String BRICK_FILE = "Brick.png";

    public static final int BLOCK_SIZE = 32;
    public static final int INIT_Y = 0;

    private BufferedImage brickImage;

    public boolean hit = false;

    public BrickBlock(int levelWidth, int levelHeight, int startPosition) {
        super(0, 0, startPosition, levelHeight - (BLOCK_SIZE * 3) - Ground.BLOCK_SIZE, 
                levelWidth - BLOCK_SIZE, levelHeight - BLOCK_SIZE - Ground.BLOCK_SIZE, 
                    BLOCK_SIZE, BLOCK_SIZE, levelWidth, levelHeight, Direction.LEFT);
    
        try {
            if (brickImage == null) {
                InputStream brickBlcok = getClass().getClassLoader().getResourceAsStream(BRICK_FILE);
                if (brickBlcok != null) {
                    brickImage = ImageIO.read(brickBlcok);
                    brickBlcok.close();
                } else {
                    throw new IOException("Error: " + BRICK_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }    
    }

    @Override
    public void move() {
    }

    @Override
    public void handleOffScreen() {
    }

    public void draw(Graphics g) {
        if (hit == false) {
            g.drawImage(brickImage, x, y, width, height, null);

        }
    }
}
