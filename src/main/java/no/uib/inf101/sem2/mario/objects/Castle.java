package no.uib.inf101.sem2.mario.objects;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class Castle extends GameObject{
    
    public static final String CASTLE_FILE = "Castle.png";

    public static final int CASTLE_WIDTH = 160;
    public static final int CASTLE_HEIGHT = 160;
    public static final int VEL_X = 0;
    public static final int VEL_Y = 0;

    public int startPosition;
    public BufferedImage castleImage;

    public Castle(int levelWidth, int levelHeight, int startPosition) {
        super(VEL_X, VEL_Y, startPosition, levelHeight - Ground.BLOCK_SIZE - CASTLE_HEIGHT,
                levelWidth, levelHeight, CASTLE_WIDTH, CASTLE_HEIGHT, levelWidth,
                    levelHeight, Direction.LEFT);

        try {
            if (castleImage == null) {
                InputStream castle = getClass().getClassLoader().getResourceAsStream(CASTLE_FILE);
                if (castle != null) {
                    castleImage = ImageIO.read(castle);
                    castle.close();
                } else {
                    throw new IOException("Error: " + CASTLE_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    @Override
    public void move() {
    }

    @Override
    public void handleOffScreen() {
    }

    public void draw(Graphics g) {
        g.drawImage(castleImage, x, y, width, height, null);
    }
}
