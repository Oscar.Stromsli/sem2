package no.uib.inf101.sem2.mario.blocks;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.objects.GameObject;
import no.uib.inf101.sem2.mario.objects.Mushroom;
import no.uib.inf101.sem2.mario.utilities.Direction;

public class QuestionBlock extends GameObject {

    
    public static final String QUESTION_BLOCK_FILE = "QuestionBlock.png";
    public static final String EMPTY_BLOCK_FILE = "EmptyBlock.png";

    public static final int BLOCK_SIZE = 32;

    public static int INIT_X; // Initial placement of question block in x

    private BufferedImage qBlockImage; // question block image

    public boolean hit = false; // boolean to check if mario has hit the question block
    public boolean empty = false;

    public QuestionBlock(int levelWidth, int levelHeight, int startPosition) {
        super(0, 0, startPosition, levelHeight - (BLOCK_SIZE * 3) - Ground.BLOCK_SIZE, 
                levelWidth - BLOCK_SIZE, levelHeight - BLOCK_SIZE - Ground.BLOCK_SIZE, 
                    BLOCK_SIZE, BLOCK_SIZE, levelWidth, levelHeight, Direction.LEFT);
        INIT_X = startPosition; // Set the initial x-coordinate
    
        try {
            if (qBlockImage == null) {
                InputStream questionBlock = getClass().getClassLoader().getResourceAsStream(QUESTION_BLOCK_FILE);
                if (questionBlock != null) {
                    qBlockImage = ImageIO.read(questionBlock);
                    questionBlock.close();
                } else {
                    throw new IOException("Error : " + QUESTION_BLOCK_FILE);
                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Mushroom createPowerUp() {
        return new Mushroom(this.x, this.y - Mushroom.MUSHROOM_SIZE, 0);
    }

    @Override
    public void move() {
        // Does not move
    }

    @Override
    public void handleOffScreen() {
        // Does not move, and should therefore not be offscreen.
    }   

    public void draw(Graphics g) {
        if (hit) {
            try {
                InputStream emptyBlock = getClass().getClassLoader().getResourceAsStream(EMPTY_BLOCK_FILE);
                if (emptyBlock != null) {
                    BufferedImage emptyBlockImage = ImageIO.read(emptyBlock);
                    g.drawImage(emptyBlockImage, x, y, width, height, null);
                    emptyBlock.close();
                } else {
                    throw new IOException("Error : " + EMPTY_BLOCK_FILE);
                }
            } catch (IOException e) {
                System.out.println("Error: " + e.getMessage());
            }
        } else if (!empty) {
            g.drawImage(qBlockImage, x, y, width, height, null);
        }
    }  
}
