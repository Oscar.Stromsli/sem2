package no.uib.inf101.sem2.mario.player;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import no.uib.inf101.sem2.mario.Ground;
import no.uib.inf101.sem2.mario.MarioView;

public class MarioController implements KeyListener{

    private Mario mario;

    public MarioController(Mario mario) {
        this.mario = mario;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Do nothing
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            mario.velX -= MarioView.MARIO_SPEED_X;

        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            mario.velX += MarioView.MARIO_SPEED_X;

        } else if (e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == KeyEvent.VK_UP) {
            if (mario.velY >= 0) {
                mario.velY -= MarioView.MARIO_SPEED_Y;
                mario.jumping = true;
            }
        }
          
        // Update Mario's position
        mario.move();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            mario.velX = 0;
            Ground.vel_x = 0;

        } else if (e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == KeyEvent.VK_UP) {
            if (!mario.jumping) { 
                mario.velY = 0; 
            }
        }
    }    
}
