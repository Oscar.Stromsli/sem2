package no.uib.inf101.sem2.mario.utilities;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
