package no.uib.inf101.sem2;

import no.uib.inf101.sem2.mario.MarioView;
// import no.uib.inf101.sem2.snake.SnakeView;
// import no.uib.inf101.sem2.view.SampleView;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    // SampleView view = new SampleView();
    // SnakeView view = new SnakeView();
    MarioView view = new MarioView();

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.getContentPane().setLayout(new BorderLayout());
    frame.getContentPane().add(view, BorderLayout.CENTER);
    frame.getContentPane().add(view.getHighScore(), BorderLayout.NORTH);
    frame.pack();
    frame.setVisible(true);
    frame.setResizable(false);
  }
}
