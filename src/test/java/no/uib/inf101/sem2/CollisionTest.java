package no.uib.inf101.sem2;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.mario.objects.Goomba;
import no.uib.inf101.sem2.mario.player.Mario;

public class CollisionTest {


    
    @Test
    public void testCollidesTop() {
        Mario mario = new Mario(500, 500);
        mario.y = 0;
        
        Goomba goomba = new Goomba(500, 500, 0);
        goomba.x = mario.width;
        goomba.y = mario.height - 1;
        
        // Goomba & Mario shouldn't collide top to bottom if
        // mario.y + mario.height > goomba.y
        for (mario.x = 0; mario.x <= goomba.x + goomba.width; mario.x++) {
            assertFalse(goomba.collidesTop(mario) && mario.collidesBottom(goomba));
            assertTrue(goomba.intersectsTop(mario) && mario.intersectsBottom(goomba));
        }
        
        goomba.y++;
        
        // Goomba & Mario should collide top to bottom if
        // mario.y + mario.height == goomba.y
        for (mario.x = 0; mario.x <= goomba.x + goomba.width; mario.x++) {
            assertTrue(goomba.collidesTop(mario) && mario.collidesBottom(goomba));
            assertTrue(goomba.intersectsTop(mario) && mario.intersectsBottom(goomba));
        }
        
        goomba.y++;
        
        // Goomba & Mario should collide top to bottom if
        // mario.y += mario.height < goomba.y
        for (mario.x = 0; mario.x <= goomba.x + goomba.width; mario.x++) {
            assertFalse(goomba.collidesTop(mario) && mario.collidesBottom(goomba));
            assertFalse(goomba.intersectsTop(mario) && mario.intersectsBottom(goomba));
        }
    }
    
    @Test
    public void testCollidesLeft() {
        Mario mario = new Mario(500, 500);
        mario.y = 0;
        
        Goomba goomba = new Goomba(500, 500, 0);
        goomba.x = mario.width - 1;
        goomba.y = mario.height;
        
        // Goomba & Mario shouldn't collide left to right if
        // mario.x + mario.width > goomba.x
        for (mario.y = 0; mario.y <= goomba.y + goomba.height; mario.y++) {
            assertFalse(goomba.collidesLeft(mario) && mario.collidesRight(goomba));
            assertTrue(goomba.intersectsLeft(mario) && mario.intersectsRight(goomba));
        }
        
        goomba.x++;
        
        // Goomba & Mario should collide left to right if
        // mario.x + mario.width == goomba.x
        for (mario.y = 0; mario.y <= goomba.y + goomba.height; mario.y++) {
            assertTrue(goomba.collidesLeft(mario) && mario.collidesRight(goomba));
            assertTrue(goomba.intersectsLeft(mario) && mario.intersectsRight(goomba));
        }
        
        goomba.x++;
        
        // Goomba & Mario shouldn't collide left to right if
        // mario.x + mario.width < goomba.x
        for (mario.y = 0; mario.y <= goomba.y + goomba.height; mario.y++) {
            assertFalse(goomba.collidesLeft(mario) && mario.collidesRight(goomba));
            assertFalse(goomba.intersectsLeft(mario) && mario.intersectsRight(goomba));
        }
    }
    
    @Test
    public void testCollidesBottom() {
        Goomba goomba = new Goomba(500, 500, 0);
        goomba.x = 0;
        goomba.y = 0;

        Mario mario = new Mario(500, 500);
        mario.x = goomba.width;
        mario.y = goomba.height - 1;
        
        // Goomba & Mario shouldn't collide bottom to top if
        // goomba.y + goomba.height > mario.y
        for (goomba.x = 0; goomba.x <= mario.x + mario.width; goomba.x++) {
            assertFalse(goomba.collidesBottom(mario) && mario.collidesTop(goomba));
            assertTrue(goomba.intersectsBottom(mario) && mario.intersectsTop(goomba));
        }
        
        mario.y++;
        
        // Goomba & Mario should collide bottom to top if
        // goomba.y + goomba.height == mario.y
        for (goomba.x = 0; goomba.x <= mario.x + mario.width; goomba.x++) {
            assertTrue(goomba.collidesBottom(mario) && mario.collidesTop(goomba));
            assertTrue(goomba.intersectsBottom(mario) && mario.intersectsTop(goomba));
        }
        
        mario.y++;
        
        // Goomba & Mario should collide bottom to top if
        // goomba.y += goomba.height < mario.y
        for (goomba.x = 0; goomba.x <= mario.x + mario.width; goomba.x++) {
            assertFalse(goomba.collidesBottom(mario) && mario.collidesTop(goomba));
            assertFalse(goomba.intersectsBottom(mario) && mario.intersectsTop(goomba));
        }   
    }
    
    @Test
    public void testCollidesRight() {
        Goomba goomba = new Goomba(500, 500, 0);
        goomba.x = 0;
        goomba.y = 0;

        Mario mario = new Mario(500, 500);
        mario.x = goomba.width - 1;
        mario.y = goomba.height;
        
        // Goomba & Mario shouldn't collide right to left if
        // goomba.x + goomba.width > mario.x
        for (goomba.y = 0; goomba.y <= mario.y + mario.height; goomba.y++) {
            assertFalse(goomba.collidesRight(mario) && mario.collidesLeft(goomba));
            assertTrue(goomba.intersectsRight(mario) && mario.intersectsLeft(goomba));
        }
        
        mario.x++;
        
        // Goomba & Mario should collide right to left it
        // goomba.x + goomba.width == mario.x
        for (goomba.y = 0; goomba.y <= mario.y + mario.height; goomba.y++) {
            assertTrue(goomba.collidesRight(mario) && mario.collidesLeft(goomba));
            assertTrue(goomba.intersectsRight(mario) && mario.intersectsLeft(goomba));
        }
        
        mario.x++;
        
        // Goomba & Mario should collide right to left if
        // goomba.x += goomba.width < mario.x
        for (goomba.y = 0; goomba.y <= mario.y + mario.height; goomba.y++) {
            assertFalse(goomba.collidesRight(mario) && mario.collidesLeft(goomba));
            assertFalse(goomba.intersectsRight(mario) && mario.intersectsLeft(goomba));
        }   
    }

    
}
